import sys
from PyQt5.QtWidgets import QApplication
from PyQt5.QtWidgets import QMainWindow
from Ui_MainWindow import Ui_MainWindow

class MainWindow:
    def __init__(self):
        self.main_win = QMainWindow()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self.main_win)

        self.ui.stackedWidget.setCurrentWidget(self.ui.page_home)
        # signals
        self.ui.Btn_Home.clicked.connect(self.showHome)
        self.ui.Btn_New.clicked.connect(self.showNew)
        self.ui.Btn_Load.clicked.connect(self.showLoad)
        self.ui.Btn_Vis.clicked.connect(self.showVis)

    def show(self):
        self.main_win.show()

    # slots
    def showHome(self):
        self.ui.stackedWidget.setCurrentWidget(self.ui.page_home)

    def showNew(self):
        self.ui.stackedWidget.setCurrentWidget(self.ui.page_new)

    def showLoad(self):
        self.ui.stackedWidget.setCurrentWidget(self.ui.page_load)

    def showVis(self):
        self.ui.stackedWidget.setCurrentWidget(self.ui.page_vis)

    # New substrate functions
    #Ui_MainWindow.pushButton_test.clicked.connect(yooo)    

def functionHello():
    print("Niklas is goood")

if __name__ == '__main__':
    app = QApplication(sys.argv)
    main_win = MainWindow()
    main_win.show()
    sys.exit(app.exec_())
